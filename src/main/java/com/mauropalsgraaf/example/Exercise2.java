package com.mauropalsgraaf.example;

import com.mauropalsgraaf.example.dependencies.Company;
import com.mauropalsgraaf.example.dependencies.Response;
import com.mauropalsgraaf.example.dependencies.CompanyNotFoundException;
import com.mauropalsgraaf.example.dependencies.CompanyServiceImpl;

public class Exercise2 {

    /**
     * You can imagine this function as a function in the controller, where we need to do 3 steps
     * - Validation
     * - "Action", in this case calling the CompanyService
     * - returning a HTTP response, in this case it's not a REST endpoint so we just return the object
     *
     * What are the different kind of scenarios that you want to test here? How many?
     *
     * This function (especially the success scenario in which we are able to get the company is not properly testable. What is wrong?
     * What are we doing differently in the bookkeeping to make sure we actually can test this?
     *
     * First try to create a test case for all the different kind of scenarios and when you notice that it's tricky,
     * refactor this code to be able to actually test it.
     *
     * Hint: You will need mocking with Mockito here (or any other mocking library, but go for Mockito).
     */
    public Response getCompany(int id) {
        if (isValidCompanyId(id)) {
            CompanyServiceImpl companyService = new CompanyServiceImpl();

            try {
                Company company = companyService.getCompany(id);
                return Response.builder().status(Response.Status.OK).entity(company).build();
            } catch (CompanyNotFoundException e) {
                return Response.builder().status(Response.Status.NOT_FOUND).build();
            }
        }

        return Response.builder().status(Response.Status.BAD_REQUEST).entity("Company id is not valid").build();
    }

    private boolean isValidCompanyId(int id) {
        return id < 1000;
    }
}
