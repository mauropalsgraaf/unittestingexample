package com.mauropalsgraaf.example.dependencies;

/**
 * This is all dummy code and only added to demonstrate the use of mocking / Mockito
 */
public class CompanyServiceImpl implements CompanyService {

    @Override
    public Company getCompany(int id) throws CompanyNotFoundException {
        System.out.println("Getting company");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Random number, either 0 or 1, equal change.
        int random = (int) Math.floor(Math.random() * 2);

        if (random == 0) {
            return new Company(id, "ING");
        }

        throw new CompanyNotFoundException();
    }
}
