package com.mauropalsgraaf.example.dependencies;

public interface CompanyService {
    public Company getCompany(int id) throws CompanyNotFoundException;
}
