package com.mauropalsgraaf.example.dependencies;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Company {
    private int Id;
    private String name;
}
