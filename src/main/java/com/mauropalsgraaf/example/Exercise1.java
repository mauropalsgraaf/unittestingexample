package com.mauropalsgraaf.example;

public class Exercise1 {

    /**
     * Test this function and make sure you tested all code paths!
     * You can use IntelliJ to test for your coverage by going to Run in the toolbar on top and click Run with Coverage
     */
    public Integer calculate(boolean bool1, int x, boolean bool2, String hello, int y) {
        if (bool1) {
            if (x > 10) {
                return x / 10;
            } else if (hello.contains("Hello!")) {
                return hello.length() + x - y;
            } else {
                if (!bool2) {
                    return 5 - x;
                }
            }
        } else {
            if (x > 100) {
                return x + y;
            } else {
                return y - x;
            }
        }

        return 100;
    }
}
